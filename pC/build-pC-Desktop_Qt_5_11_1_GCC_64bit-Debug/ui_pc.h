/********************************************************************************
** Form generated from reading UI file 'pc.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PC_H
#define UI_PC_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_pC
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QSlider *verticalSlider;
    QLabel *labell;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *pC)
    {
        if (pC->objectName().isEmpty())
            pC->setObjectName(QStringLiteral("pC"));
        pC->resize(309, 300);
        centralWidget = new QWidget(pC);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(110, 210, 89, 25));
        verticalSlider = new QSlider(centralWidget);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setGeometry(QRect(60, 40, 191, 161));
        verticalSlider->setOrientation(Qt::Vertical);
        labell = new QLabel(centralWidget);
        labell->setObjectName(QStringLiteral("labell"));
        labell->setGeometry(QRect(10, -20, 301, 81));
        pC->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(pC);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 309, 22));
        pC->setMenuBar(menuBar);
        mainToolBar = new QToolBar(pC);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        pC->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(pC);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        pC->setStatusBar(statusBar);

        retranslateUi(pC);

        QMetaObject::connectSlotsByName(pC);
    } // setupUi

    void retranslateUi(QMainWindow *pC)
    {
        pC->setWindowTitle(QApplication::translate("pC", "pC", nullptr));
        pushButton->setText(QApplication::translate("pC", "Aplicar", nullptr));
        labell->setText(QApplication::translate("pC", "Probabilidad de ruido del mensaje pC a pB", nullptr));
    } // retranslateUi

};

namespace Ui {
    class pC: public Ui_pC {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PC_H
