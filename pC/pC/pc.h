#ifndef PC_H
#define PC_H

#include <QMainWindow>

namespace Ui {
class pC;
}

class pC : public QMainWindow
{
    Q_OBJECT

public:
    explicit pC(QWidget *parent = nullptr);
    ~pC();
    int prob;

private slots:
    void on_pushButton_clicked();

    void on_verticalSlider_valueChanged(int value);

private:
    Ui::pC *ui;
};

#endif // PC_H
