#ifndef COMUNICARR_H
#define COMUNICARR_H
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
using namespace std;

#define SIZE 80



class Comunicarr
{
public:
    Comunicarr();

    struct Mensaje{
        int len;
        char msg[SIZE];
        int checksum;
        };

    int fd1, fd2;
    Mensaje mensaje;
    int posibilidad;

    void ejecu();
    void noise_msg(Mensaje &mensaje);

};

#endif // COMUNICARR_H
