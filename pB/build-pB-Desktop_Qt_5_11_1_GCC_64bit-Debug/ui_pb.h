/********************************************************************************
** Form generated from reading UI file 'pb.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PB_H
#define UI_PB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_pB
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton_Obtener;
    QListWidget *listWidget;
    QListWidget *listWidget_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *pB)
    {
        if (pB->objectName().isEmpty())
            pB->setObjectName(QStringLiteral("pB"));
        pB->resize(691, 438);
        centralWidget = new QWidget(pB);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton_Obtener = new QPushButton(centralWidget);
        pushButton_Obtener->setObjectName(QStringLiteral("pushButton_Obtener"));
        pushButton_Obtener->setGeometry(QRect(500, 300, 161, 61));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 50, 221, 321));
        listWidget_2 = new QListWidget(centralWidget);
        listWidget_2->setObjectName(QStringLiteral("listWidget_2"));
        listWidget_2->setGeometry(QRect(240, 50, 231, 321));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(260, 30, 211, 17));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(16, 30, 171, 20));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(500, 30, 121, 17));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(480, 70, 181, 21));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(500, 90, 181, 21));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(480, 130, 181, 21));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(480, 170, 181, 21));
        pB->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(pB);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 691, 22));
        pB->setMenuBar(menuBar);
        mainToolBar = new QToolBar(pB);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        pB->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(pB);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        pB->setStatusBar(statusBar);

        retranslateUi(pB);

        QMetaObject::connectSlotsByName(pB);
    } // setupUi

    void retranslateUi(QMainWindow *pB)
    {
        pB->setWindowTitle(QApplication::translate("pB", "pB", nullptr));
        pushButton_Obtener->setText(QApplication::translate("pB", "Obtener texto", nullptr));
        label->setText(QApplication::translate("pB", "\302\277Entendio?", nullptr));
        label_2->setText(QApplication::translate("pB", "Mensaje enviado por pC: ", nullptr));
        label_3->setText(QApplication::translate("pB", "INSTRUCCIONES:", nullptr));
        label_4->setText(QApplication::translate("pB", "1.  Aplicar rango de error", nullptr));
        label_5->setText(QApplication::translate("pB", "de los mensajes en pC", nullptr));
        label_6->setText(QApplication::translate("pB", "2.  Enviar mensaje en pA", nullptr));
        label_7->setText(QApplication::translate("pB", "3.  Obtener mensaje en pB", nullptr));
    } // retranslateUi

};

namespace Ui {
    class pB: public Ui_pB {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PB_H
