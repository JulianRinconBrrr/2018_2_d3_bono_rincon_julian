#ifndef PB_H
#define PB_H

#include <QMainWindow>

namespace Ui {
class pB;
}

class pB : public QMainWindow
{
    Q_OBJECT

public:
    explicit pB(QWidget *parent = nullptr);
    ~pB();

private slots:
    void on_pushButton_Obtener_clicked();

private:
    Ui::pB *ui;
};

#endif // PB_H
