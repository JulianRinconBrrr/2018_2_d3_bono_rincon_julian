#ifndef COMUNICACION_H
#define COMUNICACION_H
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <QString>
#define SIZE 80

class Comunicacion
{
public:
    Comunicacion();

    struct Mensaje{
        int len;
        char msg[SIZE];
        int checksum;
        };
    int fd2, fd3;
    char in_write[SIZE], in_read[SIZE];
    Mensaje mensaje;
    QString texto;
    QString texto_2;

    char fifo2[50] = "/tmp/fifo2";
    char fifo3[50] = "/tmp/fifo3";

    bool validate_checksum(Mensaje &msg);
    void Ejecutar();
};

#endif // COMUNICACION_H
