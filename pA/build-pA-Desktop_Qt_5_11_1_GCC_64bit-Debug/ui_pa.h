/********************************************************************************
** Form generated from reading UI file 'pa.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PA_H
#define UI_PA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_pA
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QLineEdit *lineEdit;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *pA)
    {
        if (pA->objectName().isEmpty())
            pA->setObjectName(QStringLiteral("pA"));
        pA->resize(400, 196);
        centralWidget = new QWidget(pA);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(140, 100, 89, 25));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(20, 50, 361, 25));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 181, 17));
        pA->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(pA);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 22));
        pA->setMenuBar(menuBar);
        mainToolBar = new QToolBar(pA);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        pA->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(pA);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        pA->setStatusBar(statusBar);

        retranslateUi(pA);

        QMetaObject::connectSlotsByName(pA);
    } // setupUi

    void retranslateUi(QMainWindow *pA)
    {
        pA->setWindowTitle(QApplication::translate("pA", "pA", nullptr));
        pushButton->setText(QApplication::translate("pA", "PushButton", nullptr));
        label->setText(QApplication::translate("pA", "Texto a enviar:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class pA: public Ui_pA {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PA_H
