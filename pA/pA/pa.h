#ifndef PA_H
#define PA_H

#include <QMainWindow>

namespace Ui {
class pA;
}

class pA : public QMainWindow
{
    Q_OBJECT

public:
    explicit pA(QWidget *parent = nullptr);
    ~pA();

private slots:
    void on_pushButton_clicked();

private:
    Ui::pA *ui;
};

#endif // PA_H
